# Puissance 4

## Installation
Run `npm i` in the `client` and `server` folders.

## Launch game
- Launch client by running `npm run build` and then in the client folder : 
  - `SET PORT=4444 && serve -s build` on windows
  - `PORT=4444 serve -s build` on linux
- Launch server by running `node main.js` in the server folder
  
## Play
Connect to localhost:4444 / localIP:4444 to play.